//
//  ArticlesListViewController.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 29/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticlesListViewModelProtocol.h"

@interface ArticlesListViewController : UIViewController

@property (nonatomic) id<ArticlesListViewModelProtocol> viewModel;

@end

