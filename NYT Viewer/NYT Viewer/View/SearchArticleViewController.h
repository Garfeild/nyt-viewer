//
//  SearchArticleViewController.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "ArticlesListViewController.h"

@interface SearchArticleViewController : ArticlesListViewController

@end
