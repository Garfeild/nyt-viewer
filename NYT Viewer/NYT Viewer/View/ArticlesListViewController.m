//
//  ArticlesListViewController.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 29/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "ArticlesListViewController.h"
#import "UIKit+AFNetworking.h"
#import "ArticleTableViewCell.h"
#import "LoadingMoreTableViewCell.h"


static NSString * const ArticleCellID = @"ArticleTableViewCell";
static NSString * const LoadMoreCellID = @"LoadMoreTableViewCell";


@interface ArticlesListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end


@implementation ArticlesListViewController

- (void)viewDidLoad;
{
  [super viewDidLoad];
  
  self.tableView.rowHeight = UITableViewAutomaticDimension;
  self.tableView.estimatedRowHeight = 340;
}

- (void)setViewModel:(id<ArticlesListViewModelProtocol>)viewModel;
{
  if ( _viewModel ) {
	_viewModel.articlesUpdated = nil;
  }
  _viewModel = viewModel;
  
  ArticlesListViewController * __weak weakSelf = self;
  _viewModel.articlesUpdated = ^(NSInteger newArticlesCount){
	ArticlesListViewController *strongSelf = weakSelf;
	dispatch_async(dispatch_get_main_queue(), ^{
	  [strongSelf.tableView reloadData];
	});
  };
}

- (void)setupCell:(ArticleTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
{
  id<ArticleViewModelProtocol> viewModel = [self.viewModel articleAtIndexPath:indexPath.row];
  
  cell.titleLabel.text = viewModel.title;
  [cell.titleLabel sizeToFit];
  
  cell.dateLabel.text = viewModel.publicationDate;
  [cell.dateLabel sizeToFit];
  
  cell.abstactLabel.text = viewModel.abstract;
  [cell.abstactLabel sizeToFit];

  [cell.articleImageView setImageWithURL:viewModel.imageURL placeholderImage:[UIImage imageNamed:@"Placeholder"]];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
  return self.viewModel.articlesCount == 0 ? 0 : self.viewModel.articlesCount+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
  if ( self.viewModel.articlesCount != 0 && indexPath.row == self.viewModel.articlesCount ) {
	LoadingMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:LoadMoreCellID];
	cell.titleLabel.text = @"Loading more articles...";
	[cell.activityIndicator startAnimating];
	return cell;
  }
  
  ArticleTableViewCell *cell = (ArticleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:ArticleCellID forIndexPath:indexPath];
  
  [self setupCell:cell atIndexPath:indexPath];
  
  return cell;
}


#pragma mark - UITableViewDelegate methods

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//  if ( self.viewModel.articlesCount == 0 || (self.viewModel.articlesCount != 0 && indexPath.row == self.viewModel.articlesCount ) ) {
//	return 100;
//  }
//  
//  id<ArticleViewModelProtocol> viewModel = [self.viewModel articleAtIndexPath:indexPath.row];
//  return viewModel.contentHeight;
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
  if ( !self.viewModel.isFetchingArticles && indexPath.row == self.viewModel.articlesCount ) {
	[self.viewModel fetchNextArticles];
	/* Flicking
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
	  [UIView setAnimationsEnabled:NO];
	  [tableView beginUpdates];
	  [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.viewModel.articlesCount inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
	  [tableView endUpdates];
	  [UIView setAnimationsEnabled:YES];
	});
	*/
  }
}

@end
