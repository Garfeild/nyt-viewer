//
//  SearchArticleViewController.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "SearchArticleViewController.h"

@interface SearchArticleViewController () <UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation SearchArticleViewController

#pragma mark - UISearchBarDelegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;
{
  [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
  [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar;
{
  [searchBar resignFirstResponder];
  [searchBar setShowsCancelButton:NO animated:YES];
  
  searchBar.text = @"";
  [self.viewModel searchForArticleWithString:nil];
  [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
{
  [self.viewModel searchForArticleWithString:searchText];
}

@end
