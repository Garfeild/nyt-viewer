//
//  AppDelegate.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 29/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

