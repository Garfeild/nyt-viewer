//
//  NetworkStore+Private.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#ifndef NetworkStore_Private_h
#define NetworkStore_Private_h

@interface NetworkStore ()

- (NSURL *)articlesURLWithOffset:(NSInteger)offset;


@end

#endif /* NetworkStore_Private_h */
