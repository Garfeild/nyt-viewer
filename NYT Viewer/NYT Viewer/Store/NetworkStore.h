//
//  NetworkStore.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkStore : NSObject

- (NSURLSessionDataTask *)fetchArticlesWithOffset:(NSInteger)offset callback:(void (^)(NSArray *articlesArray))callback;

- (NSURLSessionDataTask *)searchForArticleWithString:(NSString *)searchString pageNumber:(NSInteger)pageNumber callback:(void (^)(NSArray *articlesArray))callback;

- (void)downloadImage:(NSURL *)imageURL callback:(void (^)(NSURL *location))callback;

@end
