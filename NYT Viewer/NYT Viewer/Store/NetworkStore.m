//
//  NetworkStore.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "NetworkStore.h"
#import "NetworkStore+Private.h"

static NSString * const NYT_API_KEY = @"1fe6e2d9dfd142d0b33a7021c82090a3";

@interface NetworkStore ()

@property (nonatomic, readonly) NSURL *apiURL;
@property (nonatomic, readonly) NSString *mostViewedEndPoint;

@end


@implementation NetworkStore

- (NSURL *)apiURL;
{
  return [NSURL URLWithString:@"https://api.nytimes.com/svc/"];
}

- (NSString *)mostViewedEndPoint;
{
  return @"mostpopular/v2/mostviewed/all-sections/30.json";
}

- (NSURL *)articlesURLWithOffset:(NSInteger)offset;
{
  NSString *parameters = [self.mostViewedEndPoint stringByAppendingFormat:@"?api-key=%@&offset=%ld", NYT_API_KEY, (long)offset];
  NSURL *requestURL = [NSURL URLWithString:parameters relativeToURL:self.apiURL];
  return requestURL.absoluteURL;
}

- (NSString *)searchEndPoint;
{
  return @"search/v2/articlesearch.json";
}

- (NSURL *)searchURLWithString:(NSString *)searchString pageNumber:(NSInteger)pageNumber;
{
  NSString *parameters = [self.searchEndPoint stringByAppendingFormat:@"?api-key=%@&q=%@&page=%ld", NYT_API_KEY, searchString, pageNumber];
  NSURL *requestURL = [NSURL URLWithString:parameters relativeToURL:self.apiURL];
  return requestURL.absoluteURL;
}

#pragma mark - Public API

- (NSURLSessionDataTask *)fetchArticlesWithOffset:(NSInteger)offset callback:(void (^)(NSArray *articlesArray))callback;
{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
  NSURL *URL = [self articlesURLWithOffset:offset];
  NSURLSessionDataTask *task = [session dataTaskWithURL:URL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	if ( error ) {
	  NSLog(@"[NetworkStore] Network error: %@", error.localizedDescription);
	  return;
	}
	
	NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
	NSArray *parsedArray = dictionary[@"results"];
	
	if ( callback ) {
	  callback(parsedArray);
	}
	
  }];
  [task resume];
  
  return task;
}

- (NSURLSessionDataTask *)searchForArticleWithString:(NSString *)searchString pageNumber:(NSInteger)pageNumber callback:(void (^)(NSArray *articlesArray))callback;
{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
  NSURL *URL = [self searchURLWithString:searchString pageNumber:pageNumber];
  NSURLSessionDataTask *task = [session dataTaskWithURL:URL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	if ( error ) {
	  NSLog(@"[NetworkStore] Network error: %@", error.localizedDescription);
	  return;
	}
	
	NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
	NSArray *parsedArray = dictionary[@"response"][@"docs"];
	
	if ( callback ) {
	  callback(parsedArray);
	}
	
  }];
  [task resume];
  
  return task;
}

- (void)downloadImage:(NSURL *)imageURL callback:(void (^)(NSURL *location))callback;
{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
  NSURLSessionDownloadTask *task = [session downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
	if (callback) {
	  callback(location);
	}
  }];
  [task resume];
}

@end
