//
//  SearchResult.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "SearchResult.h"

@implementation SearchResult

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
{
  return [self initWithServerID:dictionary[kSearchResultServerID]
					   title:dictionary[kSearchResultTitle][@"main"]
					   abstract:dictionary[kSearchResultAbstract]
						pubDate:dictionary[kSearchResultPubDate]
							URLString:dictionary[kSearchResultURL]
					sectionName:dictionary[kSearchResultSectionName]];
}

@end
