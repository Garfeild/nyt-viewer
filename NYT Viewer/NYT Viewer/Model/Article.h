//
//  Article.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 Data format from API
 {
	"url":"string",
	"adx_keywords":"string",
	"column":"string",
	"section":"string",
	"byline":"string",
	"type":"string",
	"title":"string",
	"abstract":"string",
	"published_date":"string",
	"source":"string",
	"id":int,
	"asset_id":int,
	"views":int,
	"des_facet":["string"],
	"org_facet":["string"],
	"per_facet":["string"],
	"geo_facet":"string",
	"media":[{
 		"type":"string",
		"subtype":"string",
		"caption":"string",
		"copyright":"string",
		"media-metadata":[{"url":"string",
		"format":"string",
		"height":int,
		"width":int
 		}]
	}]
 }
 */

static NSString * const kArticleServerID = @"id";
static NSString * const kArticleTitle = @"title";
static NSString * const kArticlePubDate = @"published_date";
static NSString * const kArticleURL = @"url";
static NSString * const kArticleSectionName = @"section";
static NSString * const kArticleAbstract = @"abstract";


@class ArticleMedia;


@interface Article : NSObject

@property (nonatomic) NSString *serverID;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *abstract;
@property (nonatomic) NSString *pubDate;
@property (nonatomic) NSString *URLString;
@property (nonatomic) NSString *sectionName;
@property (nonatomic) NSSet *media;

- (instancetype)initWithServerID:(NSString *)serverID title:(NSString *)title abstract:(NSString *)abstract pubDate:(NSString *)pubDate URLString:(NSString *)URLString sectionName:(NSString *)sectionName NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (void)addMediaObject:(ArticleMedia *)object;
- (void)removeMediaObject:(ArticleMedia *)object;
- (void)addMedia:(NSSet *)objects;
- (void)removeMedia:(NSSet *)objects;

@end
