//
//  SearchResult.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "Article.h"

/*
{
  "web_url":"string",
  "snippet":"string",
  "lead_paragraph":"string",
  "abstract":null,
  "print_page":0,
  "blog":[],
  "source":"string",
  "multimedia":[ 
  {
     "width":75,
     "url":"images\/2017\/03\/07\/us\/07factcheck\/07factcheck-thumbStandard.jpg",
     "rank":0,
     "height":75,
     "subtype":"thumbnail",
     "legacy":{
       "thumbnailheight":75,
       "thumbnail":"images\/2017\/03\/07\/us\/07factcheck\/07factcheck-thumbStandard.jpg",
	  "thumbnailwidth":75
	 },
     "type":"image"
  }],
  "headline":{
	"main":"string",
	"kicker":"string",
	"print_headline":"string",
	"content_kicker":"string"
  },
  "keywords":[  ],
  "pub_date":"2017-02-01T14:20:28+0000",
  "document_type":"string",
  "news_desk":"string",
  "section_name":"string",
  "subsection_name":null,
  "byline":{  },
  "type_of_material":"string",
  "_id":"string",
  "word_count":integer,
  "slideshow_credits":null
},
*/

static NSString * const kSearchResultServerID = @"_id";
static NSString * const kSearchResultTitle = @"headline";
static NSString * const kSearchResultPubDate = @"pub_date";
static NSString * const kSearchResultURL = @"web_url";
static NSString * const kSearchResultSectionName = @"section_name";
static NSString * const kSearchResultAbstract = @"lead_paragraph";


@interface SearchResult : Article

@end
