//
//  Article.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "Article.h"


@interface Article ()

@property (nonatomic) NSMutableSet *mutableMedia;

@end


@implementation Article

- (instancetype)initWithServerID:(NSString *)serverID title:(NSString *)title abstract:(NSString *)abstract pubDate:(NSString *)pubDate URLString:(NSString *)URLString sectionName:(NSString *)sectionName;
{
  self = [super init];
  
  if ( self ) {
	self.serverID = serverID;
	self.title = title;
	self.abstract = abstract;
	self.pubDate = pubDate;
	self.URLString = URLString;
	self.sectionName = sectionName;
  }
  
  return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
{
  return [self initWithServerID:dictionary[kArticleServerID]
					   title:dictionary[kArticleTitle]
					   abstract:dictionary[kArticleAbstract]
						pubDate:dictionary[kArticlePubDate]
							URLString:dictionary[kArticleURL]
					sectionName:dictionary[kArticleSectionName]];
}


- (instancetype)init;
{
  return [self initWithServerID:nil title:nil abstract:nil pubDate:nil URLString:nil sectionName:nil];
}

- (NSMutableSet *)mutableMedia;
{
  if ( !_mutableMedia ) {
	_mutableMedia = [[NSMutableSet alloc] init];
  }
  return _mutableMedia;
}

- (NSSet *)media;
{
  return self.mutableMedia.copy;
}

- (void)addMediaObject:(ArticleMedia *)object;
{
  if ( object ) {
	[self.mutableMedia addObject:object];
  }
}

- (void)removeMediaObject:(ArticleMedia *)object;
{
  if ( object ) {
	[self.mutableMedia removeObject:object];
  }
}

- (void)addMedia:(NSSet *)objects;
{
  if ( objects ) {
	[self.mutableMedia unionSet:objects];
  }
}

- (void)removeMedia:(NSSet *)objects;
{
  if ( objects ) {
	[self.mutableMedia minusSet:objects];
  }
}

@end
