//
//  ArticleMedia.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <Foundation/Foundation.h>


static NSString * const kArticleMediaURL = @"url";
static NSString * const kArticleMediaFormat = @"format";
static NSString * const kArticleMediaWidth = @"width";
static NSString * const kArticleMediaHeight = @"height";


@interface ArticleMedia : NSObject

@property (nonatomic) NSString *URLString;
@property (nonatomic) NSString *format;
@property (nonatomic) NSNumber *width;
@property (nonatomic) NSNumber *height;
@property (nonatomic) NSString *fileLocation;

- (instancetype)initWithURLString:(NSString *)URLString format:(NSString *)format width:(NSNumber *)width height:(NSNumber *)height NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
