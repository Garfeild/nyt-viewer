//
//  ArticleMedia.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "ArticleMedia.h"


@implementation ArticleMedia

- (instancetype)initWithURLString:(NSString *)URLString format:(NSString *)format width:(NSNumber *)width height:(NSNumber *)height;
{
  self = [super init];
  
  if ( self ) {
	self.URLString = URLString;
	self.format = format;
	self.width = width;
	self.height = height;
  }
  
  return self;
}
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
{
  return [self initWithURLString:dictionary[kArticleMediaURL]
					format:dictionary[kArticleMediaFormat]
					 width:dictionary[kArticleMediaWidth]
					height:dictionary[kArticleMediaHeight]];
}

- (instancetype)init;
{
  return [self initWithURLString:nil format:nil width:0 height:0];
}

@end
