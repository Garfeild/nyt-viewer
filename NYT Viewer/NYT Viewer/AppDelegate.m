//
//  AppDelegate.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 29/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "AppDelegate.h"
#import "NetworkStore.h"
#import "ArticlesListViewController.h"
#import "SearchArticleViewController.h"
#import "ArticlesListViewModel.h"
#import "SearchArticlesViewModel.h"

static BOOL isRunningTests(void)
{
  NSDictionary* environment = [[NSProcessInfo processInfo] environment];
  return (environment[@"XCInjectBundleInto"] != nil);
}


@interface AppDelegate ()

@end


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  // Exit if is running tests
  if ( isRunningTests() ) {
	return YES;
  }
  
  NetworkStore *networkStore = [[NetworkStore alloc] init];
  UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;

  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] initWithNetworkStore:networkStore];
  
  UINavigationController *navigationController = (UINavigationController *)tabBarController.viewControllers.firstObject;
  ArticlesListViewController *viewController = (ArticlesListViewController *)navigationController.viewControllers.firstObject;
  viewController.viewModel = viewModel;
  
  networkStore = [[NetworkStore alloc] init];
  SearchArticlesViewModel *searchViewModel = [[SearchArticlesViewModel alloc] initWithNetworkStore:networkStore];
  
  navigationController = (UINavigationController *)tabBarController.viewControllers.lastObject;
  SearchArticleViewController *searchViewController = (SearchArticleViewController *)navigationController.viewControllers.firstObject;
  searchViewController.viewModel = searchViewModel;
  
  [viewModel fetchNextArticles];

  return YES;
}

@end
