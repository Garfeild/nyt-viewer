//
//  SearchResultViewModel.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 31/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArticleViewModelProtocol.h"

@class SearchResult;

@interface SearchResultViewModel : NSObject <ArticleViewModelProtocol>

- (instancetype)initWithArticle:(SearchResult *)article NS_DESIGNATED_INITIALIZER;

@end
