//
//  SearchResultViewModel+Private.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 31/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#ifndef SearchResultViewModel_Private_h
#define SearchResultViewModel_Private_h

#import "SearchResultViewModel.h"

@interface SearchResultViewModel ()

@property (nonatomic) SearchResult *article;

@end

#endif /* SearchResultViewModel_Private_h */
