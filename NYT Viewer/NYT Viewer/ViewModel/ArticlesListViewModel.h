//
//  ArticlesListViewModel.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArticlesListViewModelProtocol.h"

@class ArticleViewModel, NetworkStore;


@interface ArticlesListViewModel : NSObject <ArticlesListViewModelProtocol>

@property (nonatomic) NetworkStore *networkStore;

- (instancetype)initWithNetworkStore:(NetworkStore *)networkStore NS_DESIGNATED_INITIALIZER;

@end
