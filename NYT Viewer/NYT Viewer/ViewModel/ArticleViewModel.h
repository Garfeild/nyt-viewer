//
//  ArticleViewModel.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArticleViewModelProtocol.h"

@class Article;


@interface ArticleViewModel : NSObject <ArticleViewModelProtocol>

- (instancetype)initWithArticle:(Article *)article NS_DESIGNATED_INITIALIZER;

@end
