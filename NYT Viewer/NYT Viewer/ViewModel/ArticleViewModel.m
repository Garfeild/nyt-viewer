//
//  ArticleViewModel.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "ArticleViewModel.h"
#import "ArticleViewModel+Private.h"
#import "ArticleMedia.h"

@interface ArticleViewModel ()


@end


@implementation ArticleViewModel

@synthesize contentHeight = _contentHeight;

- (instancetype)initWithArticle:(Article *)article;
{
  self = [super init];
  
  if ( self ) {
	self.article = article;
	self.contentHeight = 339.5;
  }
  
  return self;
}

- (instancetype)init;
{
  return [self initWithArticle:nil];
}


#pragma mark - ArticleViewModelProtocol

- (NSString *)title;
{
  return self.article.title;
}

- (NSString *)abstract;
{
  return self.article.abstract;
}

- (NSString *)publicationDate;
{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];
  NSDate *date = [dateFormatter dateFromString:self.article.pubDate];
  
  [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
  return [dateFormatter stringFromDate:date];
}

- (NSURL *)imageURL;
{
  ArticleMedia *media = self.article.media.anyObject;
  NSString *cleanString = [media.URLString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
  return [NSURL URLWithString:cleanString];
}

@end
