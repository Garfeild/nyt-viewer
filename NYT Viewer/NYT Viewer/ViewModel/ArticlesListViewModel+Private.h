//
//  ArticlesListViewModel+Private.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#ifndef ArticlesListViewModel_Private_h
#define ArticlesListViewModel_Private_h

@interface ArticlesListViewModel ()

@property (nonatomic) NSInteger offset;
@property (nonatomic) NSMutableOrderedSet *articles;
@property (nonatomic) NSMutableOrderedSet *searchResults;
@property (nonatomic) BOOL fetchingArticles;
@property (nonatomic) NSURLSessionTask *currentNetworkTask;

@end


#endif /* ArticlesListViewModel_Private_h */
