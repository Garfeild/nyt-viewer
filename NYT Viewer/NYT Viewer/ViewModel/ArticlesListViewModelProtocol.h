//
//  ArticlesListViewModelProtocol.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#ifndef ArticlesListViewModelProtocol_h
#define ArticlesListViewModelProtocol_h

#import "ArticleViewModelProtocol.h"


@protocol ArticlesListViewModelProtocol <NSObject>

@property (nonatomic) void (^articlesUpdated)(NSInteger newArticlesNumber);
@property (nonatomic, readonly, getter=isFetchingArticles) BOOL fetchingArticles;

- (void)fetchNextArticles;

@property (nonatomic, readonly) NSInteger articlesCount;

- (id<ArticleViewModelProtocol>)articleAtIndexPath:(NSInteger)indexPath;


@optional

//@property (nonatomic, getter=isSearching) BOOL searching;

- (void)searchForArticleWithString:(NSString *)searchString;

@end

#endif /* ArticlesListViewModelProtocol_h */
