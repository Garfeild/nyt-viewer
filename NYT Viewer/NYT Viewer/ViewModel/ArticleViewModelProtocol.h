//
//  ArticleViewModelProtocol.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#ifndef ArticleViewModelProtocol_h
#define ArticleViewModelProtocol_h

@protocol ArticleViewModelProtocol <NSObject>

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *abstract;
@property (nonatomic, readonly) NSString *publicationDate;
@property (nonatomic) double contentHeight;
@property (nonatomic, readonly) NSURL *imageURL;

@end

#endif /* ArticleViewModelProtocol_h */
