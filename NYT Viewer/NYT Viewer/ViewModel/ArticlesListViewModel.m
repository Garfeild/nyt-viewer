//
//  ArticlesListViewModel.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "ArticlesListViewModel.h"
#import "ArticlesListViewModel+Private.h"
#import "NetworkStore.h"
#import "ArticleViewModel.h"
#import "Article.h"
#import "ArticleMedia.h"

@interface ArticlesListViewModel ()

@end


@implementation ArticlesListViewModel

@synthesize articlesUpdated = _articlesUpdated;
@synthesize articlesCount = _articlesCount;
@synthesize fetchingArticles = _fetchingArticles;

- (instancetype)initWithNetworkStore:(NetworkStore *)networkStore;
{
  self = [super init];
  
  if ( self ) {
	self.networkStore = networkStore;
	self.offset = 0;
  }
  
  return self;
}

- (instancetype)init;
{
  return [self initWithNetworkStore:nil];
}

- (NSMutableOrderedSet *)articles;
{
  if ( !_articles ) {
	_articles = [[NSMutableOrderedSet alloc] init];
  }
  
  return _articles;
}

- (void)fetchNextArticles;
{
  [self.networkStore fetchArticlesWithOffset:self.offset callback:^(NSArray *articlesArray) {
	@synchronized (self.articles) {
	  for ( NSInteger i=0; i<articlesArray.count; i++ ) {
		NSDictionary *dictionary = articlesArray[i];
		Article *article = [[Article alloc] initWithDictionary:dictionary];
		id<ArticleViewModelProtocol> viewModel = [[ArticleViewModel alloc] initWithArticle:article];
		
		NSArray *tmpArray = dictionary[@"media"];
		if ( tmpArray && [tmpArray isKindOfClass:[NSArray class]] && tmpArray.count > 0 ) {
		  NSArray *mediaDict = tmpArray.firstObject[@"media-metadata"];
		  
		  for ( NSDictionary *dict in mediaDict ) {
			if ( [dict[@"format"] isEqualToString:@"mediumThreeByTwo440"] ) {
			  ArticleMedia *mediaObject = [[ArticleMedia alloc] initWithDictionary:dict];
			  [article addMediaObject:mediaObject];
			}
		  }
		  
		  [self.articles addObject:viewModel];
		}
	  }
	  
	  self.fetchingArticles = NO;
	  if ( self.articlesUpdated ) {
		self.articlesUpdated(articlesArray.count);
	  }
	}
  }];
  
  if ( self.offset != 0 ) {
	self.fetchingArticles = YES;
  }
  
  self.offset += 20;
}

- (NSInteger)articlesCount;
{
  return self.articles.count;
}

- (ArticleViewModel *)articleAtIndexPath:(NSInteger)indexPath;
{
  return self.articles[indexPath];
}

@end
