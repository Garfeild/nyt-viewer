//
//  ArticleViewModel+Private.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#ifndef ArticleViewModel_Private_h
#define ArticleViewModel_Private_h

#import "ArticleViewModel.h"
#import "Article.h"

@interface ArticleViewModel ()

@property (nonatomic) Article *article;

@end


#endif /* ArticleViewModel_Private_h */
