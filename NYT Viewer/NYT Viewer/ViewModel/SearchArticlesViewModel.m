//
//  SearchArticlesViewModel.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "SearchArticlesViewModel.h"
#import "ArticlesListViewModel+Private.h"
#import "NetworkStore.h"
#import "SearchResultViewModel.h"
#import "SearchResult.h"
#import "ArticleMedia.h"


@interface SearchArticlesViewModel ()

@property (nonatomic) NSString *searchString;

@end


@implementation SearchArticlesViewModel

@synthesize articlesUpdated = _articlesUpdated;
@synthesize articlesCount = _articlesCount;
@synthesize fetchingArticles = _fetchingArticles;

- (void)setSearchString:(NSString *)searchString;
{
  _searchString = searchString;
  
  self.offset = 0;
}

- (void)fetchNextArticles;
{
  self.currentNetworkTask = [self.networkStore searchForArticleWithString:self.searchString pageNumber:self.offset callback:^(NSArray *articlesArray) {
	@synchronized (self.articles) {
	  if ( self.offset == 10 ) {
		[self.articles removeAllObjects];
	  }
	  
	  for ( NSInteger i=0; i<articlesArray.count; i++ ) {
		NSDictionary *dictionary = articlesArray[i];
		SearchResult *article = [[SearchResult alloc] initWithDictionary:dictionary];
		id<ArticleViewModelProtocol> viewModel = [[SearchResultViewModel alloc] initWithArticle:article];
		
		NSArray *media = dictionary[@"multimedia"];
		if ( media && [media isKindOfClass:[NSArray class]] ) {
		  for ( NSDictionary *mediaDict in media ) {
			if ( [mediaDict[@"subtype"] isEqualToString:@"thumbnail"] ) {
			  ArticleMedia *mediaObject = [[ArticleMedia alloc] initWithURLString:mediaDict[@"url"] format:mediaDict[@"format"] width:mediaDict[@"width"] height:mediaDict[@"height"]];
			  [article addMediaObject:mediaObject];
			}
		  }
		}
		
		[self.articles addObject:viewModel];
	  }
	  
	  self.fetchingArticles = NO;
	  
	  if ( self.articlesUpdated ) {
		self.articlesUpdated(articlesArray.count);
	  }
	}
  }];
  
  if ( self.offset != 0 ) {
	self.fetchingArticles = YES;
  }
  
  self.offset += 10;
}

- (NSInteger)articlesCount;
{
  return self.articles.count;
}

- (ArticleViewModel *)articleAtIndexPath:(NSInteger)indexPath;
{
  return self.articles[indexPath];
}

- (void)searchForArticleWithString:(NSString *)searchString;
{
  [self.currentNetworkTask cancel];
  self.searchString = searchString;
  
  if ( !searchString || [searchString isEqualToString:@""] ) {
	self.searchString = nil;
	[self.articles removeAllObjects];
  }
  else  {
	[self fetchNextArticles];
  }
}


@end
