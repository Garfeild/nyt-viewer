//
//  SearchArticlesViewModel.h
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArticlesListViewModel.h"


@interface SearchArticlesViewModel : ArticlesListViewModel

@end
