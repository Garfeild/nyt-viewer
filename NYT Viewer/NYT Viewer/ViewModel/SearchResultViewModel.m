//
//  SearchResultViewModel.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 31/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "SearchResultViewModel.h"
#import "SearchResultViewModel+Private.h"
#import "SearchResult.h"
#import "ArticleMedia.h"

@interface SearchResultViewModel ()


@end


@implementation SearchResultViewModel

@synthesize contentHeight = _contentHeight;
@synthesize imageURL = _imageURL;

- (instancetype)initWithArticle:(SearchResult *)article;
{
  self = [super init];
  
  if ( self ) {
	self.article = article;
	self.contentHeight = 0;
  }
  
  return self;
}

- (instancetype)init;
{
  return [self initWithArticle:nil];
}


#pragma mark - ArticleViewModelProtocol

- (NSString *)title;
{
  if ( !self.article.title || [self.article.title isEqual:[NSNull null]] || [self.article.title isEqualToString:@""] )
	return @" ";
  return self.article.title;
}

- (NSString *)abstract;
{
  if ( !self.article.abstract || [self.article.abstract isEqual:[NSNull null]] || [self.article.abstract isEqualToString:@""] )
	return @" ";
  return self.article.abstract;
}

- (NSString *)publicationDate;
{
  /* 2017-02-01T14:20:28+0000 */
  if ( !self.article.pubDate || [self.article.pubDate isEqual:[NSNull null]] || [self.article.pubDate isEqualToString:@""] ) {
	return @" ";
  }
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
  NSDate *date = [dateFormatter dateFromString:self.article.pubDate];
  
  if ( !date || [date isEqual:[NSNull null]] ) {
	return @" ";
  }
  
  [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
  return [dateFormatter stringFromDate:date];
}

- (NSURL *)imageURL;
{
  ArticleMedia *media = self.article.media.anyObject;
  NSString *cleanString = [media.URLString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
  return [NSURL URLWithString:[NSString stringWithFormat:@"https://static01.nyt.com/%@", cleanString]];
}

@end
