//
//  Tests04ArticlesListViewModel.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestNetworkStore.h"
#import "ArticlesListViewModel.h"
#import "ArticleViewModel+Private.h"


@interface Tests04ArticlesListViewModel : XCTestCase

@property (nonatomic) TestNetworkStore *networkStore;
@property (nonatomic, readonly) NSArray *testJSON;

@end


@implementation Tests04ArticlesListViewModel

- (void)setUp;
{
  [super setUp];
  self.networkStore = [[TestNetworkStore alloc] init];
}

- (void)tearDown;
{
  [super tearDown];
}

- (NSArray *)testJSON;
{
  NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"30_offset_0" ofType:@"json"];
  NSData *json = [NSData dataWithContentsOfFile:jsonPath];
  NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableLeaves error:nil];
  return jsonDictionary[@"results"];
}


#pragma mark - Tests

- (void)test00Init;
{
  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] init];
  XCTAssertNotNil(viewModel, @"ArticlesListViewModel object should be created");

}

- (void)test01DesignatedInitializer;
{
  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] initWithNetworkStore:self.networkStore];
  XCTAssertNotNil(viewModel, @"ArticlesListViewModel object should be created");
}

- (void)test02DesignatedInitializer;
{
  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] initWithNetworkStore:self.networkStore];
  XCTAssertTrue([viewModel.networkStore isEqual:self.networkStore], @"'networkStore' property should match passed object");
}

- (void)test03FetchingArticles;
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Network request should call trigger block-property"];
  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] initWithNetworkStore:self.networkStore];
  
  viewModel.articlesUpdated = ^(NSInteger newArticlesCount){
	XCTAssertEqual(newArticlesCount, 20, @"Number of fetched articles (%ld) should match reference (20)", newArticlesCount);
	[expectation fulfill];
  };
  
  [viewModel fetchNextArticles];
  [self waitForExpectationsWithTimeout:30 handler:^(NSError * _Nullable error) {
	
  }];
}

- (void)test04FetchingArticles;
{
  NSInteger __block count = 0;
  NSInteger __block articlesCount = 0;
  XCTestExpectation *expectation = [self expectationWithDescription:@"Network request should call trigger block-property"];

  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] initWithNetworkStore:self.networkStore];
  ArticlesListViewModel __weak *weakViewModel = viewModel;
  viewModel.articlesUpdated = ^(NSInteger newArticlesCount){
	count += 1;
	articlesCount += newArticlesCount;
	
	if ( count == 2 ) {
	  ArticlesListViewModel __strong *strongViewModel = weakViewModel;
	  XCTAssertEqual(articlesCount, 40, @"Number of fetched articles (%ld) should match reference (40)", articlesCount);
	  XCTAssertEqual(strongViewModel.articlesCount, 40, @"Number of fetched articles (%ld) should match reference (40)", newArticlesCount);
	  [expectation fulfill];
	  }
  };
  
  [viewModel fetchNextArticles];
  [viewModel fetchNextArticles];
  
  [self waitForExpectationsWithTimeout:30 handler:^(NSError * _Nullable error) {
	
  }];
}

- (void)test05FetchingArticles;
{
  NSInteger __block count = 0;
  XCTestExpectation *expectation = [self expectationWithDescription:@"Network request should call trigger block-property"];
  
  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] initWithNetworkStore:self.networkStore];
  ArticlesListViewModel __weak *weakViewModel = viewModel;
  viewModel.articlesUpdated = ^(NSInteger newArticlesCount){
	count += 1;
	
	if ( count == 3 ) {
	  ArticlesListViewModel __strong *strongViewModel = weakViewModel;
	  XCTAssertEqual(strongViewModel.articlesCount, 40, @"Number of fetched articles (%ld) should match reference (40)", newArticlesCount);
	  [expectation fulfill];
	}
  };
  
  [viewModel fetchNextArticles];
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
	[viewModel fetchNextArticles];
  });
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
	[viewModel fetchNextArticles];
  });
  
  [self waitForExpectationsWithTimeout:30 handler:^(NSError * _Nullable error) {
	
  }];
}

- (void)test06GettingArticleViewModel;
{
  NSDictionary *reference = self.testJSON.firstObject;
  XCTestExpectation *expectation = [self expectationWithDescription:@"Network request should call trigger block-property"];
  ArticlesListViewModel *viewModel = [[ArticlesListViewModel alloc] initWithNetworkStore:self.networkStore];
  ArticlesListViewModel __weak *weakViewModel = viewModel;

  viewModel.articlesUpdated = ^(NSInteger newArticlesCount){
	ArticlesListViewModel __strong *strongViewModel = weakViewModel;
	ArticleViewModel *articleViewModel = [strongViewModel articleAtIndexPath:0];
	
	XCTAssertTrue([articleViewModel.article.title isEqualToString:reference[kArticleTitle]], @"Article object's title should match reference");
	[expectation fulfill];
  };
  
  [viewModel fetchNextArticles];
  [self waitForExpectationsWithTimeout:30 handler:^(NSError * _Nullable error) {
	
  }];
}

@end
