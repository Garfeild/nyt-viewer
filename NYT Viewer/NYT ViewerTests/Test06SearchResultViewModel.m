//
//  Test06SearchResultViewModel.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 31/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SearchResultViewModel+Private.h"
#import "SearchResultViewModel.h"
#import "SearchResult.h"

@interface Test06SearchResultViewModel : XCTestCase

@property (nonatomic, readonly) NSArray *testJSON;

@end

@implementation Test06SearchResultViewModel

- (void)setUp;
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown;
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

- (NSArray *)testJSON;
{
  NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"Search" ofType:@"json"];
  NSData *json = [NSData dataWithContentsOfFile:jsonPath];
  NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableLeaves error:nil];
  return jsonDictionary[@"response"][@"docs"];
}

#pragma mark - Tests

- (void)test00Init;
{
  SearchResultViewModel *viewModel = [[SearchResultViewModel alloc] init];
  
  XCTAssertNotNil(viewModel, @"SearchResultViewModel object should be created");
}

- (void)test01DesignatedInitializer;
{
  SearchResult *article = [[SearchResult alloc] init];
  SearchResultViewModel *viewModel = [[SearchResultViewModel alloc] initWithArticle:article];
  XCTAssertNotNil(viewModel, @"SearchResultViewModel object should be created");
  XCTAssertTrue([viewModel.article isEqual:article], @"'article' property should match passed object");
}

- (void)test02TitleGetter;
{
  NSDictionary *referenceDict = self.testJSON.firstObject;
  SearchResult *article = [[SearchResult alloc] initWithDictionary:referenceDict];
  SearchResultViewModel *viewModel = [[SearchResultViewModel alloc] initWithArticle:article];
  XCTAssertTrue([viewModel.title isEqualToString:referenceDict[kSearchResultTitle][@"main"]], @"Title string should match refrence string");
}

- (void)test03AbstractGetter;
{
  NSDictionary *referenceDict = self.testJSON.firstObject;
  SearchResult *article = [[SearchResult alloc] initWithDictionary:referenceDict];
  SearchResultViewModel *viewModel = [[SearchResultViewModel alloc] initWithArticle:article];
  XCTAssertTrue([viewModel.abstract isEqualToString:referenceDict[kSearchResultAbstract]], @"Abstract string should match refrence string");
}

- (void)test04DateGetter;
{
  NSDictionary *referenceDict = self.testJSON.firstObject;
  NSString *referenceString = @"Feb 1, 2017";
  SearchResult *article = [[SearchResult alloc] initWithDictionary:referenceDict];
  SearchResultViewModel *viewModel = [[SearchResultViewModel alloc] initWithArticle:article];
  XCTAssertTrue([viewModel.publicationDate isEqualToString:referenceString], @"Date string should match refrence string");
}


@end
