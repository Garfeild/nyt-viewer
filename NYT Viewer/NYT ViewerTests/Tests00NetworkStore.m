//
//  Tests00NetworkStore.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestNetworkStore.h"


@interface Tests00NetworkStore : XCTestCase

@end


@implementation Tests00NetworkStore

- (void)setUp;
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown;
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

- (void)test00Init;
{
  NetworkStore *store = [[NetworkStore alloc] init];
  XCTAssertNotNil(store, @"NetworkStore object should be created");
}


- (void)test01NetworkRequest;
{
  XCTestExpectation *loadData = [self expectationWithDescription:@"Fetch data from API and call block"];
  
  NetworkStore *networkStore = [[NetworkStore alloc] init];
  
  [networkStore fetchArticlesWithOffset:0 callback:^(NSArray *articlesArray) {
	[loadData fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
  }];
}

- (void)test02ParsedJSON;
{
  XCTestExpectation *loadData = [self expectationWithDescription:@"Fetch data from API and call block with proper data"];
  
  TestNetworkStore *networkStore = [[TestNetworkStore alloc] init];
  
  [networkStore fetchArticlesWithOffset:0 callback:^(NSArray *articlesArray) {
	XCTAssertEqual(articlesArray.count, 20, @"Network store should call block and pass array with 20 items in it");
	[loadData fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
  }];
}


@end
