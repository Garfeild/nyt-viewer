//
//  Tests03ArticleViewModel.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ArticleViewModel.h"
#import "ArticleViewModel+Private.h"
#import "Article.h"


@interface Tests03ArticleViewModel : XCTestCase

@property (nonatomic, readonly) NSArray *testJSON;

@end


@implementation Tests03ArticleViewModel

- (void)setUp;
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown;
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}


#pragma mark - Tests

- (void)test00Init;
{
  ArticleViewModel *viewModel = [[ArticleViewModel alloc] init];
  
  XCTAssertNotNil(viewModel, @"ArticleViewModel object should be created");
}

- (NSArray *)testJSON;
{
  NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"30_offset_0" ofType:@"json"];
  NSData *json = [NSData dataWithContentsOfFile:jsonPath];
  NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableLeaves error:nil];
  return jsonDictionary[@"results"];
}


- (void)test01DesignatedInitializer;
{
  Article *article = [[Article alloc] init];
  ArticleViewModel *viewModel = [[ArticleViewModel alloc] initWithArticle:article];
  XCTAssertNotNil(viewModel, @"ArticleViewModel object should be created");
  XCTAssertTrue([viewModel.article isEqual:article], @"'article' property should match passed object");
}

- (void)test02TitleGetter;
{
  NSDictionary *referenceDict = self.testJSON.firstObject;
  Article *article = [[Article alloc] initWithDictionary:referenceDict];
  ArticleViewModel *viewModel = [[ArticleViewModel alloc] initWithArticle:article];
  XCTAssertTrue([viewModel.title isEqualToString:referenceDict[kArticleTitle]], @"Title string should match refrence string");
}

- (void)test03AbstractGetter;
{
  NSDictionary *referenceDict = self.testJSON.firstObject;
  Article *article = [[Article alloc] initWithDictionary:referenceDict];
  ArticleViewModel *viewModel = [[ArticleViewModel alloc] initWithArticle:article];
  XCTAssertTrue([viewModel.abstract isEqualToString:referenceDict[kArticleAbstract]], @"Abstract string should match refrence string");
}

- (void)test04DateGetter;
{
  NSDictionary *referenceDict = self.testJSON.firstObject;
  NSString *referenceString = @"Mar 25, 2017";
  Article *article = [[Article alloc] initWithDictionary:referenceDict];
  ArticleViewModel *viewModel = [[ArticleViewModel alloc] initWithArticle:article];
  XCTAssertTrue([viewModel.publicationDate isEqualToString:referenceString], @"Date string should match refrence string");
}


@end
