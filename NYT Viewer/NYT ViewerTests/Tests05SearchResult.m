//
//  Tests05SearchResult.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 31/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SearchResult.h"
#import "ArticleMedia.h"

@interface Tests05SearchResult : XCTestCase

@property (nonatomic) NSDictionary *testArticleDictionary;
@property (nonatomic) NSDictionary *testMediaDictionary;

@end

@implementation Tests05SearchResult

- (NSDictionary *)testArticleDictionary;
{
  if ( !_testArticleDictionary ) {
	NSString *serverID = @"id";
	NSDictionary *title = @{@"main": @"Title"};
	NSString *pubDate = @"2017-03-11";
	NSString *URL = @"http://google.com";
	NSString *sectionName = @"section";
	NSString *abstract = @"some text";
	
	_testArticleDictionary = @{
							   kSearchResultServerID: serverID,
							   kSearchResultTitle: title,
							   kSearchResultPubDate: pubDate,
							   kSearchResultURL: URL,
							   kSearchResultSectionName: sectionName,
							   kSearchResultAbstract: abstract
							   };
  }
  return _testArticleDictionary;
}

- (NSDictionary *)testMediaDictionary;
{
  if ( !_testMediaDictionary ) {
	NSURL *URL = [NSURL URLWithString:@"http://google.com"];
	NSString *format = @"Format";
	CGSize size = CGSizeMake(320, 480);
	_testMediaDictionary =  @{
							  @"URL": URL,
							  @"format": format,
							  @"width": @(size.width),
							  @"height": @(size.height)
							  };
  }
  return _testMediaDictionary;
}


#pragma mark - Tests

- (void)test00Init;
{
  SearchResult *article = [[SearchResult alloc] init];
  
  XCTAssertNotNil(article, @"SearchResult object should be created");
}

- (void)test01DesignatedInitializer;
{
  NSString *serverID = @"id";
  NSString *title = @"Title";
  NSString *pubDate = @"2017-01-01";
  NSString *URLString = @"http://google.com";
  NSString *sectionName = @"section";
  NSString *abstract = @"some text";
  
  SearchResult *article = [[SearchResult alloc] initWithServerID:serverID title:title abstract:abstract pubDate:pubDate URLString:URLString sectionName:sectionName];
  
  XCTAssertNotNil(article, @"SearchResult object should be created");
  XCTAssertTrue([article.serverID isEqualToString:serverID], @"'serverId' property should match original");
  XCTAssertTrue([article.title isEqualToString:title], @"'title' property should match original");
  XCTAssertTrue([article.pubDate isEqualToString:pubDate], @"'pubDate' property should match original");
  XCTAssertTrue([article.URLString isEqualToString:URLString], @"'URLString' property should match original");
  XCTAssertTrue([article.sectionName isEqualToString:sectionName], @"'sectionName' property should match original");
  XCTAssertTrue([article.abstract isEqualToString:abstract], @"'abstract' property should match original");
}

- (void)test02InitWithDictionary
{
  NSString *serverID = self.testArticleDictionary[kSearchResultServerID];
  NSString *title = self.testArticleDictionary[kSearchResultTitle][@"main"];
  NSString *pubDate = self.testArticleDictionary[kSearchResultPubDate];
  NSString *URL = self.testArticleDictionary[kSearchResultURL];
  NSString *sectionName = self.testArticleDictionary[kSearchResultSectionName];
  NSString *abstract = self.testArticleDictionary[kSearchResultAbstract];
  
  SearchResult *article = [[SearchResult alloc] initWithDictionary:self.testArticleDictionary];
  
  XCTAssertNotNil(article, @"SearchResult object should be created");
  XCTAssertTrue([article.serverID isEqualToString:serverID], @"'serverId' property should match original");
  XCTAssertTrue([article.title isEqualToString:title], @"'title' property should match original");
  XCTAssertTrue([article.pubDate isEqualToString:pubDate], @"'pubDate' property should match original");
  XCTAssertTrue([article.URLString isEqualToString:URL], @"'URL' property should match original");
  XCTAssertTrue([article.sectionName isEqualToString:sectionName], @"'sectionName' property should match original");
  XCTAssertTrue([article.abstract isEqualToString:abstract], @"'abstract' property should match original");
}

- (void)test03MediaSet;
{
  SearchResult *article = [[SearchResult alloc] initWithDictionary:self.testArticleDictionary];
  
  XCTAssertNotNil(article.media, @"'media' property shouldn't be nil");
}

- (void)test04AddMedia;
{
  SearchResult *article = [[SearchResult alloc] initWithDictionary:self.testArticleDictionary];
  ArticleMedia *media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  [article addMediaObject:media];
  
  XCTAssertTrue([article.media containsObject:media], @"'media' set should contain passed object");
}

- (void)test05RemoveMedia;
{
  SearchResult *article = [[SearchResult alloc] initWithDictionary:self.testArticleDictionary];
  ArticleMedia *media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  [article addMediaObject:media];
  [article removeMediaObject:media];
  
  XCTAssertTrue(![article.media containsObject:media], @"'media' set shouldn't contain passed object");
}

- (void)test06AddMultipleMedia;
{
  SearchResult *article = [[SearchResult alloc] initWithDictionary:self.testArticleDictionary];
  
  NSMutableSet *set = [[NSMutableSet alloc] init];
  ArticleMedia *media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  [set addObject:media];
  media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  [set addObject:media];
  
  [article addMedia:set];
  
  XCTAssertTrue([article.media isEqualToSet:set], @"'media' set should match passed set");
}

- (void)test07AddMultipleMedia;
{
  SearchResult *article = [[SearchResult alloc] initWithDictionary:self.testArticleDictionary];
  
  NSMutableSet *set = [[NSMutableSet alloc] init];
  ArticleMedia *media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  [set addObject:media];
  [article addMedia:set];
  media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  // Adding to article and then storing in set
  [article addMediaObject:media];
  [set addObject:media];
  
  XCTAssertTrue([article.media isEqualToSet:set], @"'media' set should match passed set");
}

- (void)test08RemoveMultipleMedia;
{
  SearchResult *article = [[SearchResult alloc] initWithDictionary:self.testArticleDictionary];
  
  NSMutableSet *set = [[NSMutableSet alloc] init];
  ArticleMedia *media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  [article addMediaObject:media];
  [set addObject:media];
  
  media = [[ArticleMedia alloc] initWithDictionary:self.testMediaDictionary];
  [article addMediaObject:media];
  
  [article removeMedia:set];
  
  XCTAssertEqual(article.media.count, 1, @"'media' set should contain 1 item");
  XCTAssertEqual(article.media.anyObject, media, @"'media' set should contain reference ArticleMedia object");
}

@end
