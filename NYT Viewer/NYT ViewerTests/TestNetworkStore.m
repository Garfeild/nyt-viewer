//
//  TestNetworkStore.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import "TestNetworkStore.h"
#import "NetworkStore+Private.h"


@implementation TestNetworkStore

- (NSURL *)articlesURLWithOffset:(NSInteger)offset;
{
  NSString *resourceName = [NSString stringWithFormat:@"30_offset_%ld", offset];
  return [[NSBundle mainBundle] URLForResource:resourceName withExtension:@"json"];
}

@end
