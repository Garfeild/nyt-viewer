//
//  Tests01ArticleMedia.m
//  NYT Viewer
//
//  Created by Anton Kolchunov on 30/03/17.
//  Copyright © 2017 Trivago. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ArticleMedia.h"


@interface Tests01ArticleMedia : XCTestCase

@end


@implementation Tests01ArticleMedia

- (void)setUp;
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown;
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

- (void)test00Init;
{
  ArticleMedia *media = [[ArticleMedia alloc] init];
  
  XCTAssertNotNil(media, @"ArticleMedia object should be created");
}

- (void)test01DesignatedInitializer;
{
  NSString *URLString = @"http://google.com";
  NSString *format = @"Format";
  CGSize size = CGSizeMake(320, 480);
  
  ArticleMedia *media = [[ArticleMedia alloc] initWithURLString:URLString format:format width:@(size.width) height:@(size.height)];
  
  XCTAssertNotNil(media, @"ArticleMedia object should be created");
  XCTAssertTrue([media.URLString isEqualToString:URLString], @"'URLString' property should match original");
  XCTAssertTrue([media.format isEqualToString:format], @"'format' property should match original");
  XCTAssertTrue(media.width.floatValue == size.width && media.height.floatValue == size.height, @"'width' and 'height' properties should match original");
}

- (void)test02InitWithDictionary;
{
  NSString *URLString = @"http://google.com";
  NSString *format = @"Format";
  CGSize size = CGSizeMake(320, 480);
  NSDictionary *dictionary = @{
							   @"url": URLString,
							   @"format": format,
							   @"width": @(size.width),
							   @"height": @(size.height)
							   };
  
  ArticleMedia *media = [[ArticleMedia alloc] initWithDictionary:dictionary];
  
  XCTAssertNotNil(media, @"ArticleMedia object should be created");
  XCTAssertTrue([media.URLString isEqualToString:URLString], @"'URLString' property should match original");
  XCTAssertTrue([media.format isEqualToString:format], @"'format' property should match original");
  XCTAssertTrue(media.width.floatValue == size.width && media.height.floatValue == size.height, @"'width' and 'height' properties should match original");
}


@end
